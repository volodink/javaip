FROM node:14-alpine

RUN apk update && apk upgrade && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories && \
    apk add \
      git \
      mc \
      htop \
      grep \
      chromium \
      freetype@edge \
      wqy-zenhei@edge \
      ttf-liberation@edge \
      font-noto-devanagari@edge \
      font-noto-arabic@edge \
      font-noto-bengali@edge \
      tree \
      zsh-vcs@edge \
      zsh@edge \
      alpine-sdk \
      nss@edge \
      openrc iptables ca-certificates dumb-init git-lfs openssl tzdata wget \
      bash bash-doc bash-completion \
      shadow \
      util-linux build-base gcc abuild binutils binutils-doc gcc-doc \
      cmake cmake-doc extra-cmake-modules extra-cmake-modules-doc \
      ccache ccache-doc pciutils usbutils coreutils binutils findutils grep openjdk11 graphviz

RUN npm install -g markdown-it-plantuml
RUN npm install -g @marp-team/marp-cli

USER node

RUN git config --global user.name "volodink" && git config --global user.email "volodin.konstantin@gmail.com"

RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

ENV IS_DOCKER true
