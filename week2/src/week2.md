---
marp: true
theme: lection
paginate: true


---

<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#

# Docker Compose

![bg right:50% h:70%](https://logos-world.net/wp-content/uploads/2021/02/Docker-Symbol.png)

#

#

#

#

#

#

#

##### Константин Володин


---
<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Лекция


---
<!-- _class: lead -->
<!-- _paginate: false -->

# Императивный и декларативный подход


---
__Императивное программирование__ — это парадигма программирования, для которой характерны следующие черты:

1. В исходном коде программы записываются инструкции (команды).

2. Инструкции должны выполняться последовательно.

3. Данные, получаемые при выполнении предыдущих инструкций, могут читаться из памяти последующими инструкциями.

4. Данные, полученные при выполнении инструкции, могут записываться в память.


---
__Декларативное программирование__ — парадигма программирования, в которой задается __спецификация решения задачи__, то есть __описывается конечный результат__, а не способ его достижения. 

В качестве примеров декларативных языков можно привести язык разметки HTML. При написании тегов в этом языке мы не задумываемся о том, как элементы будут отрисовываться на странице, мы просто описываем, как эта страница должна выглядеть.

__SQL__ - декларативный язык


---
<!-- _paginate: false -->
# Императивное и декларативное в Docker и Docker Compose
1. Запуск контейнеров в CLI - __императивное__

_docker run --rm -d -p 8888:80 nginx_

2. Создание описания контейнеров и docker compose файлов - __декларативное__

![bg right:45%](img/dockerfile.png)


---
![bg w:100%](https://static.1cloud.ru/img/blog/544.png)


---
![bg w:95%](https://babok-school.ru/wp-content/uploads/2022/07/mn_vs_ms_1-720x372.png)


---
![bg h:95%](https://vk-it.com/wp-content/uploads/2018/02/microservises.jpg)


---
![bg h:95%](https://testit.software/storage/temp/public/2d7/522/f60/1658764570398__688.jpeg)


---
__Docker Compose__ – это инструмент для управления __одним или более__ контейнеров, каждый из которых представляет __отдельный сервис__ и предназначен __для разворачивания связанных сервисов__. 

Данный инструмент __входит в состав Docker__.

Если в вашем проекте для работы приложения __требуется поднимать несколько сервисов__, этот инструмент вам подойдет.


---
![bg w:100%](https://www.simplilearn.com/ice9/free_resources_article_thumb/docker-yaml.JPG)


---
<!-- _class: big-code-shifted -->

```
version: "3.9"
services:
  web:
    build: .
    ports:
      - "8000:5000"
  redis:
    image: "redis:alpine"
```


---
![bg h:95%](https://net2.com/wp-content/uploads/2020/10/word-image-36.png)




---
<!-- _class: lead -->

# <!-- fit --> docker-compose != docker compose


---
# Основные команды docker-compose

1. __docker compose build__ сборка сервисов, описанных в конфигурационных файлах
2. __docker compose up__ запуск собранных сервисов из конфигурационного файла
3. __docker compose up -d__ запуск контейнеров на фоне с флагом -d
4. __docker compose down__ остановка и удаление всех сервисов, которые были запущены с помощью up
5. __docker compose stop__ остановка всех сервисов, которые были запущены с помощью up (их можно будет перезапустить docker-compose start)
6. __docker compose restart__ перезапуск всех остановленных и запущенных сервисов 


---

![bg w:50%](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXwAAACFCAMAAABv07OdAAABQVBMVEX///8LJHEUqoxQvv8Alv+C3SKv5h4AAGgAH2/AxNUAAGY4RoMAlP8AAGQyQH4AFGxEUYj19vnV1+MAGW952wAApoYADmoAjv+HjKyfpb/s7vQAG24Ao4Lo+N216RHi5e1LWI1HuHrz+vkNqYQAB2kAppFIu/+Hw/8AAFp9hah8yLY3tMsYq5CzuM1WYZKqrsW665Gg1cix3dLg8u40q/9CtP+Lzr6s3f9LuJ9ocZzFyNjP6uNBuNtKvPDo9fIen/8rpv+s1P+Yy/9pwa19vv/f7/8mOHuOlbOZ4gCL3zWa4imU4kqh5Ges53vK767a9MUYLXaP4D9vytDO8bO26oye5F8qsKc5tcAys7kmr6JIu+dgsP/F4v+g2K5ibJlfw+aJ1KOZ3Haq5DaU2n+L1pZmxt44tH5tyGF7zliI1ExbwW4uoES3AAAOC0lEQVR4nO2d+1/aShrGgSqJEQTFoHIRBbSKiILiothipeKx9mKPPdrWHs9ud7fdnv//D9gEyFzfCUm4fdR5fhNymXzz8sw770yMz9eHsnuVcqFZKwbaKtaahYNK/WU/R5RypHq5WUzphgKEzL9Teu2gIu/A0PSy0jQYU9gD9D1IFQ/q427lY1S2UrMDj25AKlCQ/AerejPVGzziXyxnx93gx6NK0Tn6Lv/C3rgb/ThUDrgj38XflPj7VsUL+i5+mfz0pXpNhF4nJNzkYNztf8gqpECouVyuePP5dqut2883X8xPoFugB2Tm41F1IKb1nP7l9vjFM1Zftz4XoRuQao77Kh6m+LDXc2u3XznuSC+Ob3SOvwx+D9or6iz54i0f8ayObzj8Ken8LlVJsehvbGKe0lYgx+xbG/fVPCwdpBj0n3sHPRH+RRq/XpRJp3M1afbu0IP4pfE7FZ3c5764RW9qi/b+lKTvTBR7PXfsAb2hFzdU8Kcq476sByGKfe7GG3pTx2Twpwrjvq6HIJr9Fsy1ur/faDQmTDUaJ/vVdXizLyj4Zb7pRE2CvV6E3L560ggZmsAy/2rsQzfgNifZO1eBYJ/7ApBvTJDYqTsA8T/OSfZOVSZyzNxnDuW+iDziX2V3+apL9s5UJ9nfshxPQrboO/wnWPwvJHtHyuo27E96k+/g56JfsnciItFh2VftDYfBT+65/o9xX9aDUFkX+n3DOfq2qpK9O+1hw9fpoVUVDnBL0Hcnkr0r1TD7NYr9Poc3FFpYaNy9fn56evr67sT4g99iYl2yd64KNh3dtqcNLYTuXp2Ruya+PW/w/KuSvVMRmU6OmjZh7D4UuvsG7X92yvIPVSV7h8JDWzrRodmHQqezwkN8u6PxL7waYfsfsl7i3pYy/AZtOKf2Rzk7WSDYPx9N0x++cD2NMh0q7hfuxFFv6RsaD0j2ToXTTJ00HbKvDYVAr+d0tyDZuxNRSCbze5L9Se+w7+jVgmTvRtjxydmTdYL9wmvnRzszhgGSvWMdoMAvEoFP2r0rmGchyd65cG9LzJYThu82kJ06lBQ5uC2CpuPGc6RcqgY5Ps7wQ3fjbuAjFtHdgplOY9wNfMxCdXydqOITne1Z70NIeRV2nRdA4PcqKUj1I1zPJKo6DWk6IxHKdfQtKPCdFRWkvAkVkwnXOZGZzmhUBJJ8Z73t7Nz2jqGjzMjaSp/e0gCPOXeUGODReiprJZpEroNcJ3Qi3C9zPhNW8vF4PK9o+fspcJuV6Y5mzm2bMNuaFqkFH9jU0Ua4ow3xNr4p7tCl+/NtMeBp6GC4fS02zmbQN3OCb1o74tb56sh1jnnXETp+phSOq36kpOKHzjGTVNuKL9o0wLi4sCqSti3cq5S0Tr4iPvSUxh4xmYzkwzMiIlENgo8OEovS32zmrW/CLPx49wtlSdw6nOUTlo8Ht4KdNsNJPy1VmeHdZ6Z7fyI94Ct+kfJC+Ikw2kgT+96UBh5WVfxH4PYwfNw+OhjmiDaw8GPdL+J28FEpH69ZQGWdEFzUSbTi0AVp3PUME/55BG0U2RQeWgDfaG0YtMJe8P15soeZxj9+b/DREAsvlEILdWDXycTZsO8qzDZ8mPDzxFZx4aGF8I3WQvR7wo8c4s93iM+9wUeBfwtYPpRIJCKqXyDW+IYIf5ukmhd2ajbwudaa6gmf2GuWvP+e4KNkh6hoWsNbONeZEcS9oSQTgkOEP01GgNoSHdoOvjrNb98bPj7XYYz42BN8NHVOrFqwtfxF4n4n85oh1IZ4i8nhXMOPK4w2BPDnaKh8d9MVhh/PtxUniHHAnMD3a12aR2HqUy/w60Cyg+ADq54y+JRqeGU7k8lMHXZTH+2Q3dgt/Pj2HCtBSk5FnREFJcGhEfz4znZbO4ca+uXGedd3AN+vddp0RbmvJ/gVHn7Vrr+9R1ed9Fvnmy0pZrLDG69b+ArgwqASrJuEBTcJwccbzEZtBghO4Hfu9Hme+rBP+Pz4Fqgt4OxabRG98aLhPQC4ocE/t3JdK/oigjE0AN83i3a+4rZ3Ar+d1WWY2+8JftkOPv8fGpdQhq9QsbYZhRKjocFXVSsIrSiOwBtC8H2H1k4qt70j+Krf51th0g6PkV/sCMPHaT6/OTon4Je8hgV/SkEU0K5wzwzCt8ZnJkRGjuAbF3/E+p4b+GlLf65acgQfGV3YSS3RLfzk4eImJUH+buWZRgRYPS+UN/oE8O+7EQRkqM7g+/mRjhv485Z+W7a07gA+cjphekHJdaqZjFBS4LoByrgMoijnBPJGHww/ge71Pbd9D/jcEAd94Ab+pKU3wa4g+JznH6Gk0Inr9D3IisHwN2NEBLS654hxia4pDB/9UjNWq6CBsT382CKd4viVJavC1Cd8J9mOZbY2A3pSw4GPuwgTFEoBNMgIcZ6/tNPWeUlBngHkp/bwtQw9vogdJoQ/PBv4F/3CF9fZCQ0HvoVbbScrswgv9GMkRrhdYeeAiqE94M9R9Rx/fDbjBf4lD/8vrqLMD7IQfNtaHdJw4F/h7tYU6j75xNG2tqNGgJ9KL/hUQU/b9nmC/5aH/1++vMAt2UHdm8CNGbmGz8w6xaGzoCyvO8w/olyIkRi+Cua1PeH7Ssh4YiWfN/jvLPgXCH6Bm8jily4giwMGh4DcwldbM5Ra0H7WuEqNJjKmEpaHq1F+YyH8iApOf/WGj3Ilw3Q8wt+dt+gj+P9E6Q5eMcXtZw0tBZkdI9eDLAfrIHBlT9U6QkDD/O4i+NomPE7pDR8ZT7uW5Qn+ex7+v4Bck+txUW/PhFkCtKFhjHA38fQhK8ALBfCFJ3IAvzvK71TlPMH/wMEPfkfwq2LTRz2uXyGTi9kr5QqI2mHAF0+8GPtz4UzX81FhKgkOCnzO4He8t9PjeIKfnud7XKCgf8LtiKfONXzUzFXSr4b5swwB/g4zzKHEX+gU1TvvoFshWpLiBL5vyfhb6Qx0PMH3AfB/f8aZPu87S/jilZXOCROLnXGLssIGnhA+vaEbz2/xZRUsPg1gygu4Fhn37PmGppPJrut6g49GWVCPi0wfeLJNxVef1K7uNw9nNMuFkxGm5Wggr8boog09tsTZTnSFUZQZOc2J8/Y2AnY6kYGPU5UYXJxyBj8RtpYK9YbPXXuESPSx6f+B4OPnsfhVU9TUpZqMJYlQVDfo0J0RhakAPpvnq2qe6UPv0SyURso6D1fvYwtrhPGABRJn8H3nVkw4gM8JSndI00e+w3e5vkVxlxdmLsg1fE5MCQCNM5KlDCk0n8pmm1xVExuPAs08OoSP5A0+1OPiYda+Tej77kV9nsZa+8Dho+lDxl/Q52y9hoOPjScJjMlGBB/3uFCmT6zWBB7BPYSNN8zlNAOHj+b/mJ4VT6gr9Bd8PR8bD7SEdUTwL3nTx4VNvGgNXKG/xK2TNQKJ9Rzf4OFvC6cTUF/A1LqByRRsPMAC2xHB37X1nWfYd6CHsjJRjcafDEdtBlmDgo+WqWmsX6NqGzM1CMDHvxJg6nFE8AHTD15i38EP/8PP/h+VtHysnXaqakTTSuCSsSuNXYFmLUSj4CU2BJspSpiEP2dtF+bzRNU61QbVkm1rF+KMO2HUDM4oo9xiX5/5/IC1PQ9/Q/SV6NKVtjFOAr6Dy8rkg7jwExKJ7c3pq1gseRXdFD3qcTQlEj3EEW42NUVe0xz6lP+VzYF7+BLQGYnDs4cpgev2Bc02NCv8SnxJ5rfvAN8hulziHzqG5GPQg9YHPt8JLn+FQn8iNNKnxJ6EIPj/xqG/7v1fvkj11Cegy8UzuWRZX7IfuNJQ6L8l3v7QkOyHp4+TQOjjwnLX9iX7oegaCP3gdwK+afuS/ZA0D4X+fwjjqcr/Ezg07UKhT460nu1L9sNSFgr9YJCAv9XHi932ivzDFVJYn8DQx+PcrZzu+T3adV2X9O1EhD5J35rN3cp5f5Vw53XGkr6NdkHjWf593WJvvsV8z8OBrdcZS/o2AkO/M9Ddsl4r6f51qln81idJX6xrmH7wK2Zv0K+5Q1jREXvp+3bCw1wq4wneUq8k1svOj7hXI9/iLenbKC0I/WCuGKAYOvSebIF+gbp0Hjt9EtH/sRZwjf9lIaWz7CV9G+GVgzT95VWavpH3HLy0PVK9CaEPBKTzCEUaD03/J0M/oKdqZRH/eiGgg+il79uJSPbpTnf5V6DIgUwVC5U9GubLermmi8h3fjOSvkiXkyL6QdZ62ih1w11qzcKBqUKzaPxpA74rSV+g7KSIfnD5f0Uu+K1b0FVP7u2tPVYpnoAo22foG8EvwO9Ckr2NrsX0g8uXP/rF7702+iREdrpMvm92vLm+8Kf6mBV4EnpnS3/5Vx/Rn3JRm3ii+sOOvsH/+9+BNVHnayddr4/70h6AaPpvePzLv/7Orbn9AeguK6JPVTR9PvhN/t9/rgbWzDvg8B5Iy3Eqhj4f/G3+y99//fx79UfnHvQKey+zYE9UdK8LBr91B9pataWv666nwJ60dhn6YPDje2AHX08VpNu70zVD3xa/DXw91bQvP0sBSk+yEuMXwjfQS7P3ouwlG/xC/AL4ut5j0kVKLNb4xZkPAF9P1WQ324/SFxB+YNjFwNd1XTzTJeVUnwD6fPwT8A3u5hSXzG8GofRHGD/1C1hebc+mpFJ6rVmuS/CD0/W8CL+hizemfvuzXK5U6nsS++C1a4ff1Hx63E18xMp+sscv4Q9VWdvol/CHreuPQv4S/vCVNtwH5C/hj0Qf3k0C/CX8USm9e8n+ACT8USr9/u1F+z03Ev6YlP7wfvfd28sLCd+b/g+w/ub/KGncBAAAAABJRU5ErkJggg==)


---
![bg h:95%](https://images.g2crowd.com/uploads/attachment/file/1239543/420.png)


---

![bg w:100%](https://i0.wp.com/neptune.ai/wp-content/uploads/2022/10/cnvrg-dashboard.png?ssl=1)


---

![bg w:100%](https://clear.ml/wp-content/uploads/2022/03/image61-1-768x378.png)

---

![bg h:99%](https://clear.ml/docs/latest/assets/images/ClearML_Server_Diagram-7ea19db8e22a7737f062cce207befe38.png)




---
![bg w:99%](https://d33wubrfki0l68.cloudfront.net/26a177ede4d7b032362289c6fccd448fc4a91174/eb693/images/docs/container_evolution.svg)


---

![bg h:110%](https://www.cncf.io/wp-content/uploads/2020/09/Kubernetes-architecture-diagram-1-1-1024x698.png)


---
<!-- _class: lead -->

# Совет - изучайте Kubernetes ;)

![bg right:60% h:21%](https://kubernetes.io/images/kubernetes-horizontal-color.png)


---
<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Практика

---
<!-- _class: big-code -->

# Проверяем наличие Docker compose

```bash
$ docker compose
```

---
<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> live coding session


---
<!-- _class: lead -->


# <!-- fit --> https://github.com/volodink/reactjs-fastapi-url-shortener


---

# Задача на сегодня

1. Изучить сервис
2. Запустить сервис руками
3. Написать Docker-файлы
4. Написать docker-compose.yml
5. Протестировать решение


---

# Ссылки на ресурсы

1. http://onreader.mdl.ru/UsingDocker/content/Ch04.html
2. https://infostart.ru/1c/articles/1795989/
3. https://clear.ml/
4. https://docs.docker.com/compose/
5. https://javarush.com/quests/lectures/jru.module2.lecture37
6. https://www.cncf.io/blog/2019/08/19/how-kubernetes-works/
7. 


---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!
