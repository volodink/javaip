---
marp: true
theme: lection
paginate: true
---

<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#

# Принципы проектирования ПО. Часть 2.

#

#

#

#

#

#

#

##### Константин Володин

---

<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Лекция

---

<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Часть 1. SOLID

---

<!-- _paginate: false -->

# Принципы SOLID

**SOLID** — это аббревиатура, обозначающая первые пять принципов объектно-ориентированного программирования, сформулированные **Робертом С. Мартином** (также известным как **дядя Боб**).

Эти принципы устанавливают **практики**, помогающие создавать программное обеспечение, которое можно **обслуживать** и **расширять** по мере развития проекта.

Применение этих практик поможет избавиться от **плохого кода**, **оптимизировать код**.

![bg right:30%](img/robert.jpg)

---

<!-- _class: oneimage -->

# Книги

![h:485px](img/books.png)

---

# Принципы SOLID

1. **SRP - Single Responsibility Principle**
   Принцип единственной ответственности

2. **OCP - Open/Closed**
   Принцип открытости/закрытости

3. **LSP - Liskov Substitution**
   Принцип подстановки (Барбары) Лисков

4. **ISP - Interface Segregation**
   Принцип разделения интерфейса

5. **DIP - Dependency Inversion**
   Принцип инверсии зависимостей

---

# 1. SRP - Принцип единственной ответственности

Single Responsibility Principle

<br>
<br>

## У класса должна быть одна и только одна причина для изменения, то есть у класса должна быть только одна работа.

<br>
<br>

Получаем **God Оbject** (богоподобный объект) - если все в одном классе.

**God Оbject** - антипаттерн.

---

<!-- _class: big-code -->

# Плохо

```java
public class Animal {
    private String name;
    public Animal(String name) {
        this.name = name;
    }
    String getAnimalName() {
        return this.name;
    }
    void saveAnimal(Animal animal) {
        // do some DB write stuff
    }
}
```

---

<!-- _class: big-code -->

# Хорошо

```java
public class Animal {
    private String name;
    public Animal(String name) {
        this.name = name;
    }
    String getAnimalName() {
        return this.name;
    }
}
public class AnimalDAO {
    void saveAnimal(Animal animal) {
        // do some DB write stuff
    }
}
```

---

Стив Фентон:

«Проектируя классы, мы должны стремиться к тому, чтобы объединять родственные компоненты, то есть такие, изменения в которых происходят по одним и тем же причинам. Нам следует стараться разделять компоненты, изменения в которых вызывают различные причины».

Правильное применение принципа единственной ответственности приводит к высокой степени связности элементов внутри модуля, то есть к тому, что задачи, решаемые внутри него, хорошо соответствуют его главной цели.

---

# 2. OCP - Принцип открытости/закрытости

Open/Closed Principle

<br>

## Объекты или сущности должны быть открыты для расширения, но закрыты для изменения.

## Это означает, что у нас должна быть возможность расширять класс без изменения самого класса.

---

<!-- _class: big-code -->

# Плохо

```java
List<Animal> animalList = new ArrayList<>();
animalList.add(new Animal('lion'));
animalList.add(new Animal('mouse'));

...

String animalSound(List<Animal> animalList) {
    for (var animal : animalList) {
        if (animal.getAnimalName().equals("lion")) {
            return "roar";
        }
        if (animal.getAnimalName().equals("mouse")) {
            return "squeak";
        }
    }
}
```

---

<!-- _class: big-code -->

# Совсем плохо

```java
List<Animal> animalList = new ArrayList<>();
animalList.add(new Animal("lion"));
animalList.add(new Animal("mouse"));
animalList.add(new Animal("snake"));

...

String animalSound(List<Animal> animalList) {
    for (var animal : animalList) {
        if (animal.getAnimalName().equals("lion")) {
            return "roar";
        }
        if (animal.getAnimalName().equals("mouse")) {
            return "squeak";
        }
        if (animal.getAnimalName().equals("snake")) {
            return "hiss";
        }
    }
}
```

---

<!-- _class: big-code -->

# Хорошо

```java
public class Animal {
    abstract String makeSound();
}
public class Lion extends Animal {
    String makeSound() { return "roar"; }
}
public class Mouse extends Animal {
    String makeSound() { return "squeak"; }
}
public class Snake extends Animal {
    String makeSound() { return "hiss"; }
}
```

---

<!-- _class: big-code -->

```java
String animalSound(List<Animal> animalList) {
    for (var animal : animalList)
        animal.makeSound();
}
```

---

# 3. LSP - Принцип подстановки (Барбары) Лисков

Liskov Substitution

<br>

## Пусть q(x) будет доказанным свойством объектов x типа T. Тогда q(y) будет доказанным свойством объектов y типа S, где S является подтипом T.

Это означает, что каждый подкласс или производный класс должен быть заменяемым на базовый класс или родительский класс.

Subtypes should be replaceble by their base types.
Подклассы могли бы служить заменой для своих суперклассов.

---

Robert C. Martin summarizes it:

Subtypes must be substitutable for their base types.

---

<!-- _class: big-code -->

# Плохо

```java
public class MediaPlayer {
    // Play audio implementation
    public void playAudio() {
        System.out.println("Playing audio...");
    }
    // Play video implementation
    public void playVideo() {
        System.out.println("Playing video...");
    }
}
```

---
<!-- _class: big-code -->

```java
public class VlcMediaPlayer extends MediaPlayer {}
public class WinampMediaPlayer extends MediaPlayer {
    // Play video is not supported in Winamp player
    public void playVideo() {
        throw new VideoUnsupportedException();
    }
}
public class VideoUnsupportedException extends RuntimeException {
    private static final long serialVersionUID = 1L;
}
```


---
![bg h:90%](img/d1.png)


---
![bg h:90%](img/d2.png)


---
# 4. ISP - Принцип разделения интерфейса

Interface Segregation

<br>

## Клиент никогда не должен быть вынужден реализовывать интерфейс, который он не использует, или клиенты не должны вынужденно зависеть от методов, которые они не используют.


---
![bg w:90%](img/int1.webp)


---
# 5. DIP - Принцип инверсии зависимостей

Dependency Inversion

<br>

## Сущности должны зависеть от абстракций, а не от чего-то конкретного. Это означает, что модуль высокого уровня не должен зависеть от модуля низкого уровня, но они оба должны зависеть от абстракций.

Этот принцип открывает возможности декаплинга (decoupling) или понижения связности.


---
<!-- _class: big-code -->

# Плохо

```java
public class OrderProcessor {
    public void process(Order order){

        MySQLOrderRepository repository = new MySQLOrderRepository();
        ConfirmationEmailSender mailSender = new ConfirmationEmailSender();

        if (order.isValid() && repository.save(order)) {
            mailSender.sendConfirmationEmail(order);
        }
    }

}
```

---
<!-- _class: big-code -->

```java
public class MySQLOrderRepository {
    public boolean save(Order order) {
        MySqlConnection connection = new MySqlConnection("database.url");
        // сохраняем заказ в базу данных

        return true;
    }
}

public class ConfirmationEmailSender {
    public void sendConfirmationEmail(Order order) {
        String name = order.getCustomerName();
        String email = order.getCustomerEmail();

        // Шлем письмо клиенту
    }
}
```

---
<!-- _class: big-code -->

# Хорошо

```java
public interface MailSender {
    void sendConfirmationEmail(Order order);
}

public interface OrderRepository {
    boolean save(Order order);
}
```


---
<!-- _class: big-code -->

# Хорошо

```java
public class ConfirmationEmailSender implements MailSender {
    @Override
    public void sendConfirmationEmail(Order order) {
        String name = order.getCustomerName();
        String email = order.getCustomerEmail();
        // Шлем письмо клиенту
    }
}

public class MySQLOrderRepository implements OrderRepository {
    @Override
    public boolean save(Order order) {
        MySqlConnection connection = new MySqlConnection("database.url");
        // сохраняем заказ в базу данных
        return true;
    }
}
```


---
<!-- _class: big-code -->


```java
public class OrderProcessor {
    private MailSender mailSender;
    private OrderRepository repository;
    public OrderProcessor(MailSender mailSender, OrderRepository repository) {
        this.mailSender = mailSender;
        this.repository = repository;
    }
    public void process(Order order){
        if (order.isValid() && repository.save(order)) {
            mailSender.sendConfirmationEmail(order);
        }
    }
}
```



---

<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Практика

---

# Задание лр / на неделю / ДЗ

1. Прочитать доступные источники по теме занятия

2. Поднять индивидуальное задание дисциплины ООП

3. Для каждого из принципов изучить реализацию на предмет наличия его нарушения.

4. Для каждого из принципов предложить улучшение кода.

---

# Ссылки на ресурсы

1. https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design-ru

2. https://www.youtube.com/watch?v=pTB30aXS77U

3. https://github.com/ArjanCodes/betterpython/tree/main/9%20-%20solid

4. https://stackoverflow.com/questions/31317141/whats-the-difference-between-design-patterns-and-design-principles#:~:text=A%20principle%20is%20an%20abstraction,that%20solves%20a%20particular%20problem.

5. https://khalilstemmler.com/articles/object-oriented/programming/4-principles/#:~:text=The%20four%20main%20principles%20of,%2C%20encapsulation%2C%20and%20polymorphism).

---

# Ссылки на ресурсы

6. https://www.digitalocean.com/community/tutorials/what-is-dry-development

7. https://whitelabelcoders.com/blog/best-practices-in-programming-based-on-solid-kiss-and-personal-experience/

8. https://henriquesd.medium.com/dry-kiss-yagni-principles-1ce09d9c601f

9. https://www.redhat.com/en/blog/balancing-if-it-aint-broke-dont-fix-it-vs-release-early-and-often

10. https://medium.com/webbdev/solid-4ffc018077da

---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!

---

<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Часть 2. GRASP

---

# GRASP

**GRASP - general responsibility assignment software patterns** — общие шаблоны распределения ответственностей.

GRASP шаблоны, используемые в объектно-ориентированном проектировании для решения общих задач по назначению ответственностей классам и объектам.

---

# GRASP состоит из 5 основных и 4 дополнительных шаблонов.

Основные шаблоны:

- Information Expert, Creator, Controller, Low Coupling, High Cohesion

Дополнительные шаблоны:

- Pure Fabrication, Indirection, Polymorphism, Protected Variations
