---
marp: true
theme: lection
paginate: true


---
<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#

# Принципы проектирования ПО. Часть 1.

# 

#

#

#

#

#

#

##### Константин Володин


---
# Задание на неделю 1

В здании 9 этажей и 3 лифта, которые вызываются одной кнопкой. 

На первом всегда должен быть свободный лифт. 

По нажатию на кнопку на любом этаже должен приехать лифт, расположенный наиболее близко.

---
# Задание на неделю 2

Разработайте и докажите корректность работы алгоритма простой скользящей средней (Simple Moving Average, SMA), Экспоненциальное скользящее среднее(Exponential moving average), медианного фильтра. 

Требования к программе:
1. Данные на вход поступают по одному значению.
2. Размер окна задается пользователем. 
3. Графики значений до и после обработки постройте любым доступным способом.
4. Количество исходных значений не менее 25.
5. Способы ввода: загрузка из интернета, загрузка из файла.


---
<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Лекция

---
# Принципы программирования

__Abstraction principle__
__Black box__
__DRY, KISS, YAGNI__
__If it ain't broke, don't fix it__
__Rule of three__
__Worse is better__
__Ninety–ninety rule__
__Deutsch limit__
__Avoid premature optimization__
__GRASP__
__SOLID__

Offtopic: __Теорема CAP__, __ACID__ and __BASE__


---
# Abstraction principle

![bg w:102%](https://d33wubrfki0l68.cloudfront.net/57d0ec4dbd6a798e516156554448171285453a5f/5903d/img/blog/object-oriented/programming/4-principles/abstraction-oo.png)


---

![bg w:99%](https://d33wubrfki0l68.cloudfront.net/2be01046dcded55cb57fa42da0c4c5b33681a96b/fb8b2/img/blog/object-oriented/programming/4-principles/washing-machine-abstraction.png)


---
# Black box

__Black box тестирование__ – это функциональное и нефункциональное тестирование __без доступа к внутренней структуре компонентов системы__. 

__Метод тестирования «черного ящика»__ – процедура получения и выбора тестовых случаев на основе анализа спецификации (функциональной или нефункциональной), компонентов или системы __без ссылки на их внутреннее устройство__.

---

![bg w:95%](https://upload.wikimedia.org/wikipedia/commons/7/7c/Blackbox3D-withGraphs.png)


---

![bg w:95%](https://www.researchgate.net/profile/Niall-O-Mahony/publication/331586553/figure/fig1/AS:753501407809536@1556660143025/a-Traditional-Computer-Vision-workflow-vs-b-Deep-Learning-workflow-Figure-from-8.jpg)


---

![bg](https://cdn.careerist.com/public/constructor-assets-static/articles-static/belyy-seryy-i-chernyy-yashchik/1.jpeg)


---

![bg w:90%](img/Ranorex-_Black-Box-Testing.png)

---
# DRY

__DRY__, which stands for __‘don’t repeat yourself,’__ is a principle of software development that aims at __reducing the repetition of patterns and code duplication in favor of abstractions and avoiding redundancy__.

Popularized by the book, The Pragmatic Programmer, the DRY principle states that, 

_“every piece of knowledge must have a single, unambiguous, authoritative representation within a system.”_ 

Using the principle, logic or algorithms that have certain functionality __should only appear once in an application__.

---
# KISS

__KISS__ is a term created in the 1960’s which was meant to reflect the approach of creating simple durable aircrafts that could be repaired easily in an emergency. This idea has since been adopted by many branches of science and engineering.

__KISS__ is a term also used in developing solutions to programming problems. Literally translated, KISS means “keep it simple, stupid” or “keep it stupid simple“.

However, it’s assumed, the statement was not supposed to sound negative, rather to suggest __a simple design makes service and maintenance child’s play__.

---
# YAGNI

__YAGNI__ stands for __You Ain’t Gonna Need It__. 

It’s a principle from software development methodology of Extreme Programming (XP). This principle says that __you should not create features that it’s not really necessary__.

This principle it’s similar to the KISS principle, once that both of them aim for a simpler solution. 

The difference between them it’s that YAGNI focus on removing unnecessary functionality and logic, and KISS focus on the complexity.

---

Ron Jeffries, one of the co-founders of the XP, once said:

_Always implement things when you actually need them, never when you just foresee that you need them._

It means that you should not implement functionality just because you think that you may need it someday, but implement it just when you really need it. 

Doing that you will avoid spending time with implementations that were not even necessary, and maybe will never be used.

---
# If it ain't broke, don't fix it

1. Pain from change
2. Pain from staying static
3. Pain from technical debt
4. Everyone hates change

__Solution__
Embrace innovation by making it an integral part of business operations. 

![](https://www.redhat.com/rhdc/managed-files/styles/wysiwyg_full_width/private/balancing-image1.png?itok=DN4jQRvP)



![bg right h:135%](https://external-preview.redd.it/VztnrmRy3GGxbJNpfWRqxeRbA3WhsWh-j-ir1w6M25s.jpg?width=640&crop=smart&auto=webp&v=enabled&s=ec91911b192f263eba1dc6ef02c7ab6ac21b4a66)


---
# Rule of three

__"Three strikes and you refactor"__ - is a code refactoring rule of thumb to decide when similar pieces of code should be refactored to avoid duplication. 

It states that two instances of similar code do not require refactoring, but when similar code is used three times, it should be extracted into a new procedure. 

The rule was popularised by Martin Fowler in Refactoring and attributed to Don Roberts.


---
# Worse is better

__Worse is better__ (also called the New Jersey style) is a term conceived by Richard P. Gabriel in an essay of the same name to describe the dynamics of software acceptance. 

It refers to the argument that software quality does not necessarily increase with functionality: 
 - that there is a point where less functionality ("worse") is a preferable option ("better") in terms of practicality and usability. 
 
Software that is limited, but simple to use, may be more appealing to the user and market than the reverse.


---
# Ninety–ninety rule

The first 90 percent of the code accounts for the first 90 percent of the development time. 

The remaining 10 percent of the code accounts for the other 90 percent of the development time.

— Tom Cargill, Bell Labs


---
# Deutsch limit

The Deutsch limit is an aphorism about the information density of visual programming languages originated by L. Peter Deutsch that states:

The problem with visual programming is that you can’t have more than 50 visual primitives on the screen at the same time.


---

![bg w:88%](https://gekoms.org/wp-content/uploads/2020/05/razrabotka_scada.jpg)

---
# Premature optimization

![h:500px](img/opt.webp)


---
# Теорема CAP

В распределенной системе возможно выбрать только 2 из 3-х свойств:

__C (consistency) — согласованность__. Каждое чтение даст вам самую последнюю запись.

__A (availability) — доступность__. Каждый узел (не упавший) всегда успешно выполняет запросы (на чтение и запись).

__P (partition tolerance) — устойчивость к распределению__. Даже если между узлами нет связи, они продолжают работать независимо друг от друга.


---

![bg h:95%](https://habrastorage.org/getpro/habr/post_images/5ea/6e0/5c6/5ea6e05c6e882b2c695570a487f7f1d0.gif)


---
# ACID

1. __Atomicity — Атомарность__

2. __Consistency — Согласованность__

3. __Isolation — Изолированность__

4. __Durability — Надёжность__

---
# Atomicity — Атомарность

Атомарность гарантирует, что каждая транзакция будет выполнена полностью или не будет выполнена совсем. 

Не допускаются промежуточные состояния.


---
# Consistency — Согласованность

Транзакция, достигающая своего нормального завершения (EOT — end of transaction, завершение транзакции) и, тем самым, фиксирующая свои результаты, сохраняет согласованность базы данных. 

Другими словами, каждая успешная транзакция по определению фиксирует только допустимые результаты.


---
# Isolation — Изолированность

Во время выполнения транзакции параллельные транзакции не должны оказывать влияния на её результат.


---
# Durability — Надёжность

Если пользователь получил подтверждение от системы, что транзакция выполнена, он может быть уверен, что сделанные им изменения не будут отменены из-за какого-либо сбоя. 

Обесточилась система, произошел сбой в оборудовании? 

На выполненную транзакцию это не повлияет.


---
# BASE

__BASE__ (__Basically Available, Soft-state, Eventually consistent — базовая доступность, неустойчивое состояние, согласованность в конечном счёте__), при этом такой подход напрямую противопоставляется ACID.

---
# BASE

__Basically Available__ – Rather than enforcing immediate consistency, BASE-modelled NoSQL databases will ensure availability of data by spreading and replicating it across the nodes of the database cluster.

__Soft State__ – Due to the lack of immediate consistency, data values may change over time. The BASE model breaks off with the concept of a database which enforces its own consistency, delegating that responsibility to developers.

__Eventually Consistent__ – The fact that BASE does not enforce immediate consistency does not mean that it never achieves it. However, until it does, data reads are still possible (even though they might not reflect the reality).


---
# Немного терминологии

## 1. Класс

A class — in the context of Java — is a template used to create objects and to define object data types and methods. Classes are categories, and objects are items within each category. All class objects should have the basic class properties.

## 2. Абстрактный класс

An abstract class is nothing but a class that is declared using the abstract keyword. It also allows us to declare method signatures using the abstract keyword (abstract method) and forces its subclasses to implement all the declared methods.


---
# Немного терминологии

## 3. Интерфейс

An __interface__ is a __description of the actions__ that an __object__ can do.


An interface is a __fully abstract class__. 
It includes a group of abstract methods (methods without a body). 
We use the __interface keyword__ to create an interface in Java. 
For example, 
```java
interface Language { 
    public void getType(); 
    public void getVersion();
}
interface Drawable { 
    public void draw(); 
}
```


---
# Немного терминологии

## 4. Concrete class

A concrete class is a class that we can create __an instance of__, using the __new keyword__. 

In other words, it's a full implementation of its blueprint. A concrete class is complete.


---
# GRASP

__GRASP - general responsibility assignment software patterns__ — общие шаблоны распределения ответственностей. 

GRASP шаблоны, используемые в объектно-ориентированном проектировании для решения общих задач по назначению ответственностей классам и объектам.

---
# GRASP состоит из 5 основных и 4 дополнительных шаблонов.

Основные шаблоны:
 - Information Expert, Creator, Controller, Low Coupling, High Cohesion

Дополнительные шаблоны:
 - Pure Fabrication, Indirection, Polymorphism, Protected Variations

---

<!-- _paginate: false -->

# Принципы SOLID

__SOLID__ — это аббревиатура, обозначающая первые пять принципов объектно-ориентированного программирования, сформулированные __Робертом С. Мартином__ (также известным как __дядя Боб__).

Эти принципы устанавливают __практики__, помогающие создавать программное обеспечение, которое можно __обслуживать__ и __расширять__ по мере развития проекта. 

Применение этих практик поможет избавиться от __плохого кода__, __оптимизировать код__.

![bg right:30%](img/robert.jpg)


---
<!-- _class: oneimage -->

# Книги

![h:485px](img/books.png)


---
# Принципы SOLID

1) __SRP - Single Responsibility Principle__ 
Принцип единственной ответственности

2) __OCP - Open/Closed__ 
Принцип открытости/закрытости

3) __LSP - Liskov Substitution__
Принцип подстановки (Барбары) Лисков

4) __ISP - Interface Segregation__ 
Принцип разделения интерфейса

5) __DIP - Dependency Inversion__ 
Принцип инверсии зависимостей


---
# 1. SRP - Принцип единственной ответственности
Single Responsibility Principle

<br>
<br>

## У класса должна быть одна и только одна причина для изменения, то есть у класса должна быть только одна работа.

<br>
<br>

Получаем __God Оbject__ (богоподобный объект) - если все в одном классе.

__God Оbject__ - антипаттерн.


---
# 2. OCP - Принцип открытости/закрытости
Open/Closed Principle

<br>

## Объекты или сущности должны быть открыты для расширения, но закрыты для изменения.

## Это означает, что у нас должна быть возможность расширять класс без изменения самого класса.


---
# 3. LSP - Принцип подстановки (Барбары) Лисков
Liskov Substitution

<br>

## Пусть q(x) будет доказанным свойством объектов x типа T. Тогда q(y) будет доказанным свойством объектов y типа S, где S является подтипом T.

Это означает, что каждый подкласс или производный класс должен быть заменяемым на базовый класс или родительский класс.


---
# 4. ISP - Принцип разделения интерфейса
Interface Segregation 

<br>

## Клиент никогда не должен быть вынужден реализовывать интерфейс, который он не использует, или клиенты не должны вынужденно зависеть от методов, которые они не используют.


---
# 5. DIP - Принцип инверсии зависимостей
Dependency Inversion 

<br>

## Сущности должны зависеть от абстракций, а не от чего-то конкретного. Это означает, что модуль высокого уровня не должен зависеть от модуля низкого уровня, но они оба должны зависеть от абстракций.

Этот принцип открывает возможности декаплинга (decoupling) или понижения связности.


---
# Ссылки на ресурсы

1. https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design-ru

2. https://www.youtube.com/watch?v=pTB30aXS77U

3. https://github.com/ArjanCodes/betterpython/tree/main/9%20-%20solid

4. https://stackoverflow.com/questions/31317141/whats-the-difference-between-design-patterns-and-design-principles#:~:text=A%20principle%20is%20an%20abstraction,that%20solves%20a%20particular%20problem.

5. https://khalilstemmler.com/articles/object-oriented/programming/4-principles/#:~:text=The%20four%20main%20principles%20of,%2C%20encapsulation%2C%20and%20polymorphism).

---
# Ссылки на ресурсы

6. https://www.digitalocean.com/community/tutorials/what-is-dry-development

7. https://whitelabelcoders.com/blog/best-practices-in-programming-based-on-solid-kiss-and-personal-experience/

8. https://henriquesd.medium.com/dry-kiss-yagni-principles-1ce09d9c601f

9. https://www.redhat.com/en/blog/balancing-if-it-aint-broke-dont-fix-it-vs-release-early-and-often




---
# Задание лр / на неделю / ДЗ

1. Прочитать доступные источники по теме занятия

2. Реализовать задачи 1 и 2

2. Для каждого из принципов изучить полученную реализацию на предмет наличия его нарушения.

3. Для каждого из принципов предложить улучшение кода задачи 1 и 2.


---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!
