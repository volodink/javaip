---
marp: true
theme: lection
paginate: true
---

<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#

# Паттерны проектирования

## Часть 2. 

#

#

#

#

#

#

##### Константин Володин

---

<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Лекция

---

# Builder

![bg w:95%](img/problem1.png)


---

![bg w:95%](img/problem2.png)


---

<!-- _class: big-code -->

Use the Builder pattern to get rid of a “telescoping constructor”.

```java
class Pizza {
    Pizza(int size) { ... }
    Pizza(int size, boolean cheese) { ... }
    Pizza(int size, boolean cheese, boolean pepperoni) { ... }
    // ...
```

---

![bg h:95%](img/solution1.png)

---

![bg h:95%](img/example-en.png)


---

# Adapter

Use the Adapter class when you want to use some existing class, but its interface isn’t compatible with the rest of your code.


---

![bg w:95%](img/problem-en-adapter.png)


---

![bg h:95%](img/solution-en-adapter.png)


---

![bg h:95%](img/example-adapter.png)

---

<!-- _class: big-code -->

```java
// Say you have two classes with compatible interfaces:
// RoundHole and RoundPeg.
class RoundHole is
    constructor RoundHole(radius) { ... }
    method getRadius() is
        // Return the radius of the hole.
    method fits(peg: RoundPeg) is
        return this.getRadius() >= peg.getRadius()

class RoundPeg is
    constructor RoundPeg(radius) { ... }
    method getRadius() is
        // Return the radius of the peg.

// But there's an incompatible class: SquarePeg.
class SquarePeg is
    constructor SquarePeg(width) { ... }
    method getWidth() is
        // Return the square peg width.
```

---

<!-- _class: big-code -->

```java
// An adapter class lets you fit square pegs into round holes.
// It extends the RoundPeg class to let the adapter objects act
// as round pegs.
class SquarePegAdapter extends RoundPeg is
    // In reality, the adapter contains an instance of the
    // SquarePeg class.
    private field peg: SquarePeg

    constructor SquarePegAdapter(peg: SquarePeg) is
        this.peg = peg

    method getRadius() is
        // The adapter pretends that it's a round peg with a
        // radius that could fit the square peg that the adapter
        // actually wraps.
        return peg.getWidth() * Math.sqrt(2) / 2
```

---

<!-- _class: big-code -->

```java
// Somewhere in client code.
hole = new RoundHole(5)
rpeg = new RoundPeg(5)
hole.fits(rpeg) // true

small_sqpeg = new SquarePeg(5)
large_sqpeg = new SquarePeg(10)
hole.fits(small_sqpeg) // this won't compile (incompatible types)

small_sqpeg_adapter = new SquarePegAdapter(small_sqpeg)
large_sqpeg_adapter = new SquarePegAdapter(large_sqpeg)
hole.fits(small_sqpeg_adapter) // true
hole.fits(large_sqpeg_adapter) // false
```

---

# Iterator

Use the Iterator pattern when your collection has a complex data structure under the hood, but you want to hide its complexity from clients (either for convenience or security reasons).


![w:1000](img/problem1-iterator.png)


---

![bg w:95%](img/problem2-traversa.png)


---

![bg h:95%](img/solution1-iterator.png)


---

![bg h:90%](img/example-iterator.png)


---

<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Практика


---

# Задание лр / на неделю / ДЗ

Найти место где применить и реализовать паттерны в своем проекте.

1. Builder

2. Adapter

3. Iterator


---

# Ссылки на ресурсы

1. https://habr.com/ru/post/244521/

2. https://refactoring.guru/design-patterns/iterator

3. https://refactoring.guru/design-patterns/adapter

4. https://refactoring.guru/design-patterns/builder


---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!
