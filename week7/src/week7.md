---
marp: true
theme: lection
paginate: true

---

<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#

# DI && DI:

# Dependency inversion

# Dependency injection

#

#

#

#

##### Константин Володин


---

<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Лекция


---

# Coupling and Cohesion

Тwo key concepts in software engineering that are used to measure the quality of a software system’s design.


---

# Cohesion

Cohesion in Java is the Object-Oriented principle most closely associated with making sure that a class is designed with a single, well-focused purpose. In object-oriented design, cohesion refers all to how a single class is designed. 

Cohesion refers to the degree to which elements within a module work together to fulfill a single, well-defined purpose. High cohesion means that elements are closely related and focused on a single purpose, while low cohesion means that elements are loosely related and serve multiple purposes.


---

# Coupling

Coupling refers to the degree of interdependence between software modules. High coupling means that modules are closely connected and changes in one module may affect other modules. Low coupling means that modules are independent and changes in one module have little impact on other modules.


---

<!-- _class: diagram -->

![bg h:90%](img/cohesion.png)


---

__Cohesion__ is an indication of how related and focused the responsibilities of an software element are.

__Coupling__ refers to how strongly a software element is connected to other elements.

The software element could be __class, package, component, subsystem or a system__. And while designing the systems it is recommended to have software elements that have __High cohesion__ and support __Low coupling__.

__Low cohesion__ results in monolithic classes that are difficult to maintain, understand and reduces re-usablity. 

Similarly __High Coupling__ results in classes that are tightly coupled and changes tend not be non-local, difficult to change and reduces the reuse.

---

# Inversion of Control (IoC)

Inversion of Control (IoC) is a design principle (although, some people refer to it as a pattern). 

As the name suggests, it is used to invert different kinds of controls in object-oriented design to achieve loose coupling. 

Here, controls refer to any additional responsibilities a class has, other than its main responsibility.

This include control over the flow of an application, and control over the flow of an object creation or dependent object creation and binding.

---

# Dependency Inversion

Dependency inversion is a design principle. 

In simple terms, it means we should not depend on low-level implementations, but rather rely on high-level abstractions. 

And that makes sense, as it allows us to be agnostic towards the implementation details. 

Whether we want to change a specific implementation or support other implementations, the code will stay intact, as it’s decoupled from the low-level implementation details.


---

![bg w:95%](https://www.tutorialsteacher.com/Content/images/ioc/ioc-step2.png)


---

<!-- _class: big-code -->

```java
function preparePizza() {
 const grandmasPizzaRecipe = GrandmasPizzaRecipe()
 const grandmaPizzaPreparer = GrandmaPizzaPreparer()
 
 return grandmaPizzaPreparer.preparePizza(grandmasPizzaRecipe)
}

function preparePizza(recipe: IPizzaRecipe, preparer: IPizzaPreparer) {
  return preparer.preparePizza(recipe)
}
```


---

# Внедрение зависимостей - Dependency Injection

«Внедрение зависимостей» — это выражение, впервые использованное в статье Мартина Фаулера Inversion of Control Containers and the Dependency Injection Pattern.

Внедрение зависимостей — это стиль настройки объекта, при котором поля объекта задаются внешней сущностью. 

Другими словами, объекты настраиваются внешними объектами. 

DI — это альтернатива самонастройке объектов. 


---

# Dependency Injection

Dependency injection is a design pattern that allows us to separate creation from use. 

It allows us to “inject” the required objects at run-time, without worrying about constructing them ourselves. 

It also tends to work hand in hand with the dependency inversion principle.


---

<!-- _class: big-code -->

```java
public class MyDao {
    //в оригинале: protected DataSource dataSource =
    private DataSource dataSource =
    new DataSourceImpl("driver", "url", "user", "password");
    //data access methods...
    public Person readPerson(int primaryKey) {...}
}
```

---

<!-- _class: big-code -->

```java
public class MyDao {

  //в оригинале: protected DataSource dataSource = null;
  private final DataSource dataSource;

  public MyDao(String driver, String url, String user, String password){
    this.dataSource = new DataSourceImpl(driver, url, user, password);
  }

  //data access methods...
  public Person readPerson(int primaryKey) {...}

}
```


---

<!-- _class: big-code -->

```java
public class MyDao {

    //в оригинале:  protected DataSource dataSource = null;
    private final DataSource dataSource;
    
    public MyDao(DataSource dataSource){
      this.dataSource = dataSource;
    }

    //data access methods...
    public Person readPerson(int primaryKey) {...}

  }
```


---

# Выгоды от использования DI и DI-контейнеров

1. Меньше зависимостей
2. Меньше «перенос» зависимостей
3. Код проще переиспользовать
4. Код удобнее тестировать
5. Код удобнее читать


---

# Advantages of dependency injection

1. Dependency injection relieves various code modules from the task of instantiating references to resources and enables dependencies -- even mock dependencies -- to be swapped out easily. By enabling the framework to do the resource creation, configuration data is centralized, and updates only occur in one place.
2. Injected resources can be customized through Extensible Markup Language files outside the source code. This enables changes to be applied without having to recompile the entire codebase.

---

3. Programs are more testable, maintainable and reusable. This is possible because the client's knowledge of how its dependencies are implemented is removed.
4. It enables developers to work on more. Developers can create classes independently that still use each other. They only need to know the interface of the classes.
5. Dependency injection helps in unit testing directly as well. It can externalize configuration details into configuration files. This enables the system to be reconfigured without recompiling.


---

# Альтернативы внедрению зависимостей

Альтернативой внедрению зависимостей является использование локатора служб. 

Шаблон проектирования локатора служб также улучшает отделение классов от конкретных зависимостей. 

Вы создаете класс, известный как локатор сервисов, который создает и хранит зависимости, а затем предоставляет эти зависимости по запросу.


---

<!-- _class: big-code -->

```kotlin
object ServiceLocator {
    fun getEngine(): Engine = Engine()
}
class Car {
    private val engine = ServiceLocator.getEngine()
    fun start() {
        engine.start()
    }
}
fun main(args: Array) {
    val car = Car()
    car.start()
}
```

---

<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Практика


---

# Задание лр / на неделю / ДЗ

Для произвольного проекта на Java применить принцип инверсии зависимости и продемонстрировать код до и после изменений.

---

# Ссылки на ресурсы

1. [https://www.geeksforgeeks.org/software-engineering-coupling-and-cohesion/](https://www.geeksforgeeks.org/software-engineering-coupling-and-cohesion/)
2. [https://betterprogramming.pub/straightforward-simple-dependency-inversion-vs-dependency-injection-7d8c0d0ed28e](https://betterprogramming.pub/straightforward-simple-dependency-inversion-vs-dependency-injection-7d8c0d0ed28e)
3. [https://www.tutorialsteacher.com/ioc/dependency-inversion-principle](https://www.tutorialsteacher.com/ioc/dependency-inversion-principle)
4. [https://habr.com/ru/post/350068/#start](https://habr.com/ru/post/350068/#start)
5. [https://apptractor.ru/info/articles/dependency-injection.html](https://apptractor.ru/info/articles/dependency-injection.html)
6. [https://python-dependency-injector.ets-labs.org/introduction/di_in_python.html](https://python-dependency-injector.ets-labs.org/introduction/di_in_python.html)



---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!
