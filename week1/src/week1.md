---
marp: true
theme: lection
paginate: true

---

<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#

# Docker

![bg right:50% h:70%](https://logos-world.net/wp-content/uploads/2021/02/Docker-Symbol.png)

#

#

#

#

#

#

#

##### Константин Володин


---
<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Лекция

---

![bg h:95%](https://bigwater.consulting/wp-content/uploads/2019/04/SDLC_BWC.png)


---

![bg h:95%](https://aristeksystems.com/static/d9b2d9991208a559ba1d21eaf19928fd/1dc7e/SDLC.webp)


---
<!-- _paginate: false -->

# Отличие от виртуальных машин

![bg w:95%](img/docker1.png)


---

![bg](https://infostart.ru/upload/iblock/26a/26a552862ae9a76e04cc60bdc7366a81.jpg)


---
# Для чего нужен:

1. Изолированный запуск приложений в контейнерах.
2. Упрощение разработки, тестирования и деплоя приложений.
3. Отсутствие необходимости конфигурировать среду для запуска — она поставляется вместе с приложением — в контейнере.
4. Упрощает масштабируемость приложений и управление их работой с помощью систем оркестрации контейнеров.


---
<!-- _paginate: false -->

![bg](https://eternalhost.net/wp-content/uploads/2019/12/CHto-takoe-Docker-komponenty-Docker-Engine.png)


---
<!-- _paginate: false -->

![bg h:110%](http://onreader.mdl.ru/UsingDocker/content/figures/Fig0401.jpg)


---

# Образ

1. Read-only шаблон с набором инструкций, предназначенных для создания контейнера
2. Образ состоит из неизменяемых слоев, каждый из которых добавляет/удаляет/изменяет файлы из предыдущего слоя.
3. Неизменяемость слоев позволяет их использовать совместно в разных образах.

![h:280px](img/images.png)


---

# Контейнеры

Контейнер - это запущенный образ.


---
<!-- _class: big-code -->

# Документация

```url
https://docs.docker.com/reference/
```

Описание Dockerfile, docker-compose.yml, синтаксиса команд docker и docker-compose


---
<!-- _class: lead -->
<!-- _paginate: false -->

# <!-- fit --> Практика

---
<!-- _class: big-code -->

# Проверяем наличие Docker

```bash
$ docker -v
```

---
<!-- _class: big-code -->

# Установка Docker

```url
https://docs.docker.com/engine/install/ubuntu/
https://docs.docker.com/engine/install/linux-postinstall/
```

---
<!-- _class: big-code -->

# Удалим все, что Docker качал и все контейнеры

```bash
$ docker system prune --all
```

---
<!-- _class: big-code -->

# Запускаем существующий контейнер

```bash
$ docker run hello-world
```

---
<!-- _class: big-code -->

# Dockerhub

```url
https://hub.docker.com/
```

1. Зарегистрируйтесь
2. Не забывайте логин и пароль :)

---
<!-- _class: big-code -->

# Запускаем существующий контейнер

```bash
$ docker run hello-world
```

---
<!-- _class: big-code -->

# Запускаем контейнер пользователя volodink

1. Выполним вот эту команду

```bash
$ docker run volodink/hello-world:1.0
```

2. А теперь вот эту

```bash
$ docker run volodink/hello-world:2.0
```

Что поменялось?

---
<!-- _class: big-code -->

# Список образов

```bash
$ docker images
```

# Cписок работающих контейнеров
```bash
$ docker ps
```

# Cписок всех контейнеров
```bash
$ docker ps -a
```

---
<!-- _class: big-code -->

# Что делает docker при запуске образа?

```bash
$ docker run --rm -p 8888:80 nginx
```
1. скачивает образ
2. создает контейнер
3. инициализирует файловую систему и монтирует read-only образ
4. инициализирует сеть/мост
5. запускает указанный процесс
6. обрабатывает и выдает вывод приложения


---

# Ссылки на ресурсы

1. http://onreader.mdl.ru/UsingDocker/content/Ch04.html
2. https://infostart.ru/1c/articles/1795989/
3. 

---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!
