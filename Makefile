all: clean prepare
	cd week1 && make && cp dist/week1.pdf ../dist/week1.pdf
	cd week2 && make && cp dist/week2.pdf ../dist/week2.pdf
	cd week3 && make && cp dist/week3.pdf ../dist/week3.pdf
	cd week4 && make && cp dist/week4.pdf ../dist/week4.pdf
	cd week6 && make && cp dist/week6.pdf ../dist/week6.pdf
	cd week7 && make && cp dist/week7.pdf

clean:
	rm -rf dist

prepare:
	mkdir -p dist

gitpod-bootstrap:
	sudo apt-get update && sudo apt-get dist-upgrade -y
